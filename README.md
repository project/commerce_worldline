# Commerce Worldline

Provides integration with the ATOS Worldline payment provider.

# Development

To change the base URL of the SIPS notification callback, which defaults to the `commerce_payment` defined endpoint, you can add this to your
`settings.php` file:

```
$settings['commerce_worldline_base_url'] = 'https://new-base-url.com';
```
