<?php

/**
 * @file
 * Commerce Worldline API documentation.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allows altering of the payment request.
 */
function hook_commerce_worldline_payment_request_alter(\Sips\PaymentRequest $request, $config, \Drupal\commerce_order\Entity\OrderInterface $order) {
  // Overrides the SIPS endpoint.
  $request->setSipsUri('https://sips.example');

  // Define a billing contact phone numbger.
  $request->setBillingContactPhone('+33123456789');
}

/**
 * @} End of "addtogroup hooks"
 */
