<?php

namespace Drupal\Tests\commerce_worldline\Unit;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_worldline\TransformOrder;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\Language;
use Drupal\Tests\UnitTestCase;
use Drupal\user\UserInterface;
use Sips\PaymentRequest;

/**
 * Test the TransformOrder class.
 *
 * @group commerce_worldline
 */
class TransformOrderTest extends UnitTestCase {

  /**
   * The relevant part of the plugin configuration array.
   *
   * @var array
   */
  protected $config;

  /**
   * The payment.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentInterface
   */
  protected $payment;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $order;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->config = [
      'sips_passphrase' => 'llama',
      'mode' => 0,
      'sips_merchant_id' => 'owl',
      'sips_key_version' => 1,
    ];

    $this->order = $this->createMock(OrderInterface::class);
    $this->order->expects($this->any())
      ->method('getTotalPrice')
      ->willReturn(new Price('2000', 'EUR'));
    $user = $this->createMock(UserInterface::class);
    $user->expects($this->any())
      ->method('getPreferredLangcode')
      ->willReturn('nl');
    $this->order->expects($this->any())
      ->method('getCustomer')
      ->willReturn($user);
  }

  /**
   * Tests basic transformation.
   */
  public function testTransformation() {
    $moduleHandler = $this->createMock(ModuleHandlerInterface::class);
    $testTransformer = new TransformOrder($moduleHandler);

    $transformed = $testTransformer->toPaymentRequest($this->config, $this->order, 'http://www.dazzle.be', 12, 'http://commerce.example/notify', 'VISA');

    $this->assertInstanceOf(PaymentRequest::class, $transformed);
    $this->assertEquals(200000, $transformed->toArray()['amount']);
    $this->assertEquals('nl', $transformed->toArray()['customerLanguage']);
    $this->assertEquals('VISA', $transformed->toArray()['paymentMeanBrandList']);
    $this->assertEquals('owl', $transformed->toArray()['merchantId']);
    $this->assertEquals('http://www.dazzle.be', $transformed->toArray()['normalReturnUrl']);
    $this->assertEquals('http://commerce.example/notify', $transformed->toArray()['automaticResponseUrl']);
    $this->assertEquals('1', $transformed->toArray()['keyVersion']);
  }

  /**
   * Tests the customer language.
   */
  public function testCustomerLanguage() {
    $moduleHandler = $this->createMock(ModuleHandlerInterface::class);
    $testTransformer = new TransformOrder($moduleHandler);

    $order = $this->createMock(OrderInterface::class);
    $order->expects($this->atLeastOnce())
      ->method('getTotalPrice')
      ->willReturn(new Price('2000', 'EUR'));
    $nl_order = clone $order;
    $user = $this->createMock(UserInterface::class);
    $user->expects($this->any())
      ->method('getPreferredLangcode')
      ->willReturn('nl');
    $nl_order->expects($this->atLeastOnce())
      ->method('getCustomer')
      ->willReturn($user);

    $transformed = $testTransformer->toPaymentRequest($this->config, $nl_order, 'http://www.dazzle.be', 1);
    $this->assertEquals('nl', $transformed->toArray()['customerLanguage']);

    $en_order = clone $order;
    $user = $this->createMock(UserInterface::class);
    $user->expects($this->any())
      ->method('getPreferredLangcode')
      ->willReturn('en');
    $en_order->expects($this->atLeastOnce())
      ->method('getCustomer')
      ->willReturn($user);

    $transformed = $testTransformer->toPaymentRequest($this->config, $en_order, 'http://www.dazzle.be', 2);
    $this->assertEquals('en', $transformed->toArray()['customerLanguage']);

    $it_order = clone $order;
    $user = $this->createMock(UserInterface::class);
    $user->expects($this->any())
      ->method('getPreferredLangcode')
      ->willReturn('it');
    $it_order->expects($this->atLeastOnce())
      ->method('getCustomer')
      ->willReturn($user);

    $transformed = $testTransformer->toPaymentRequest($this->config, $it_order, 'http://www.dazzle.be', 3);
    $this->assertEquals('it', $transformed->toArray()['customerLanguage']);

    $invalid_lang_order = clone $order;
    $user = $this->createMock(UserInterface::class);
    $user->expects($this->any())
      ->method('getPreferredLangcode')
      ->willReturn('milk');
    $invalid_lang_order->expects($this->atLeastOnce())
      ->method('getCustomer')
      ->willReturn($user);

    $transformed = $testTransformer->toPaymentRequest($this->config, $invalid_lang_order, 'http://www.dazzle.be', 4);
    $this->assertEquals('en', $transformed->toArray()['customerLanguage']);
  }

  /**
   * Tests no brand selection.
   */
  public function testNoBrandSelection() {
    $moduleHandler = $this->createMock(ModuleHandlerInterface::class);
    $testTransformer = new TransformOrder($moduleHandler);

    $transformed = $testTransformer->toPaymentRequest($this->config, $this->order, 'http://www.dazzle.be', 4);
    $this->assertInstanceOf(PaymentRequest::class, $transformed);
    $this->assertArrayNotHasKey('paymentMeanBrandList', $transformed->toArray());
  }

  /**
   * Tests the alteration of the payment request.
   */
  public function testPaymentRequestAlteration() {
    $moduleHandler = $this->createMock(ModuleHandlerInterface::class);
    $moduleHandler
      ->method('alter')
      ->will($this->returnCallback(function ($hook, PaymentRequest $request, $config, OrderInterface $order) {
        $request->setSipsUri('https://sips.example');
        $request->setAmount(6);
      }));
    $testTransformer = new TransformOrder($moduleHandler);

    $transformed = $testTransformer->toPaymentRequest($this->config, $this->order, 'http://www.dazzle.be', 4);
    $this->assertEquals($transformed->getSipsUri(), 'https://sips.example');
    $this->assertEquals($transformed->getAmount(), 6);

  }

}
